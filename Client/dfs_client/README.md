###Client With RMI

This client connects to a server and through RMI it provides the following functionality:

* Retrieve files list
* Download file
* Upload file
* Delete file

Initially it creates a folder for the user's files. All file operations use this folder.