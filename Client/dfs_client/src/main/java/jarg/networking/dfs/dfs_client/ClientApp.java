package jarg.networking.dfs.dfs_client;
/**
 * This will be the main client application for 
 * a distributed file system. It will be using a terminal 
 * with options. The user will be able to upload their files to the
 * DFS, download them and share them (allow access to other users).
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Scanner;

import jarg.networking.dfs.DFS_Backend.general.clientUtils.ClientRedirectedRequests_Intf;
import jarg.networking.dfs.DFS_Backend.general.clientUtils.ClientRequests_Intf;
import jarg.networking.dfs.DFS_Backend.general.clientUtils.Document;


public class ClientApp {
    private static int userID = -1;
    private static String username;
    private static String password;
    private static String root_folder = "Client_Files" + File.separator;
    private static ClientRequests_Intf main_connection;
    private static Registry main_registry;
    private static ClientRedirectedRequests_Intf redirect_connection;
    private static Registry redirect_registry;
    private static int retries = 10;

    public static void main( String[] args ){

        if(args.length > 0) {
            System.out.println( "Client started.." );
            //Setup security policy options for RMI
            System.setProperty("java.security.policy", DefaultValues.RESOURCES_FOLDER + "security.policy");
            if (System.getSecurityManager() == null) {
                System.setSecurityManager(new SecurityManager());
            }

            while (retries > 0) {
                try {
                    //Initialize your personal directory if it doesn't exist
                    initializeClientDirectory();
                    //Connect to main remote server to be able to send requests
                    main_registry = LocateRegistry.getRegistry("localhost", Integer.parseInt(args[0]));
                    main_connection = (ClientRequests_Intf) main_registry.lookup("CL_Requests_Main");

                    //Start operation *****************************************************************
                    //Manage your account
                    int option = accountManagement();
                    //Manage client requests
                    if (option != 0) {
                        requestManager();
                    }
                    break;
                } catch (Exception e) {
                    if (--retries <= 0) System.err.println(e.getMessage());
                }
            }
            retries = 10;
        }
        System.out.println("Goodbye!");       
    }
    
    //Functions for starting/connecting client ==============================================

    //Create the directory where the user will have its files
    private static void initializeClientDirectory() {
        while (retries > 0) {
            try {
                Files.createDirectory(Paths.get(root_folder));
                break;
            } catch (FileAlreadyExistsException e) {
                break;
                //No need to print anything
            } catch (IOException e) {
                if (--retries <= 0) System.err.println(e.getMessage());
            }
        }
        retries = 10;
    }
    
    //Handle Log in / Sign up
    private static int accountManagement() {
    	int option;
        Scanner keyInput = new Scanner(System.in);
        
    	do {
        	System.out.println("Welcome, type one of the following options.");
        	System.out.println("1) Log in.");
        	System.out.println("2) Create account.");
        	System.out.println("0) Exit.");
        	option = keyInput.nextInt();
        	
        	switch(option) {
                case 1 :
        			System.out.println("Type your username : ");
        			keyInput.nextLine(); //consume any "\n"
                    username = keyInput.nextLine();
                    System.out.println("Type your password : ");
	                password = keyInput.nextLine();
                    while (retries > 0) {
                        try {
                            userID = main_connection.logIn(username, password);
                            if (userID > 0) {
                                System.out.println("Log in complete.");
                                return 1;
                            } else {
                                System.out.println("Wrong credentials.");
                            }
                            break;
                        } catch (RemoteException e) {
                            if (--retries <= 0) System.err.println(e.getMessage());
                        }
                    }
                    break;
        		case 2 :
        			System.out.println("Type a username : ");
        			keyInput.nextLine(); //consume any "\n"
                    username = keyInput.nextLine();
                    System.out.println("Type a password : ");
                    password = keyInput.nextLine();
                    while (retries > 0) {
                        try {
                            userID = main_connection.createAccount(username, password);
                            if (userID > 0) {
                                System.out.println("Sing up complete. You are logged in.");
                                System.out.println("User ID : " + userID);
                                return 1;
                            } else {
                                System.out.println("Wrong credentials.");
                            }
                            break;
                        } catch (RemoteException e) {
                            if (--retries <= 0) System.err.println(e.getMessage());
                        }
                    }
                    break;
        		default :
        			break;
        	}
        	retries = 10;
        }while(option != 0);
    	
    	keyInput.close();
    	
    	return option;
    }
    
    //Functions for client operations ===================================================================
    
    //Connect to the correct redirect-server for your operations
    private static void connectToServer(String server_address) {
    	String server_ip = server_address.split(":")[0];
    	int server_port = Integer.parseInt(server_address.split(":")[1]);

        while (retries > 0) {
            try {
                redirect_registry = LocateRegistry.getRegistry(server_ip, server_port);
                redirect_connection = (ClientRedirectedRequests_Intf) redirect_registry.lookup("CL_Requests_Redirected");
                break;
            } catch (RemoteException | NotBoundException e) {
                //System.out.println("Unable to contact the server, retrying... " + retries);
                if (--retries <= 0) System.err.println(e.getMessage());
            }
        }
        retries = 10;
    	//System.out.println("Redirected to <" + server_address + ">");
    }
    
    //Show the list of files
    private static void showFileList() {
        while (retries > 0) {
            try {
                ArrayList<String> files = main_connection.retrieveFileListFromDB(userID);
                for (String file : files) {
                    System.out.println(file);
                }
                break;
            } catch (RemoteException e) {
                //System.out.println("Something went wrong, retrying... " + retries);
                if (--retries <= 0) System.err.println(e.getMessage());
            }
        }
        retries = 10;
    }
    
    //Download a file
    private static void downloadFile(String filename) {
        while (retries > 0) {
            try {
                //First ask which server to connect to for the operation
                String redirect_server = main_connection.donwloadRedirect(userID, filename);
                connectToServer(redirect_server);
                //Then download from that server
                Document doc = redirect_connection.donwloadFile(userID, filename);
                if (doc != null) {
                    while (retries > 0) {
                        try (FileOutputStream out_file = new FileOutputStream(root_folder + doc.getFilename())) {
                            out_file.write(doc.getData());
                            System.out.println("Download complete.");
                            break;
                        } catch (IOException e) {
                            //System.out.println("Something went wrong, retrying... " + retries);
                            if (--retries <= 0) System.err.println(e.getMessage());
                        }
                    }
                    retries = 10;
                } else {
                    System.out.println("The file does not exist.");
                }
                break;
            } catch (RemoteException e) {
                //System.out.println("Something went wrong, retrying... ");
                if (--retries <= 0) System.err.println(e.getMessage());
            }
        }
        retries = 10;
    }
    
    //Upload a file
    private static void uploadFile(String filename) {
        while (retries > 0) {
            try {
                //First ask which server to connect to for the operation
                String redirect_server = main_connection.uploadRedirect(userID, filename);
                connectToServer(redirect_server);
                //Then upload a file to server
                Path path = Paths.get(root_folder + filename);
                if (Files.exists(path)) {
                    Document upDoc = new Document(userID, filename, path, true);
                    boolean success = redirect_connection.uploadFile(userID, upDoc);
                    if (success) {
                        System.out.println("Upload complete.");
                    } else {
                        System.out.println("There was a problem with the upload.");
                    }

                } else {
                    System.out.println("File does not exist.");
                }
                break;
            } catch (RemoteException e) {
                //System.out.println("Something went wrong, retrying... " + retries);
                if (--retries <= 0) System.err.println(e.getMessage());
            }
        }
        retries = 10;
    }
	    
    //Delete a file
    private static void deleteFile(String filename) {
        while (retries > 0) {
            try {
                //First ask which server to connect to for the operation
                String redirect_server = main_connection.deleteRedirect(userID, filename);
                if(!redirect_server.equals("")) {
                	 connectToServer(redirect_server);
                	//Then delete a file from the server
                     boolean success = redirect_connection.deleteFile(userID, filename);
                     if (success) {
                         System.out.println("File deleted.");
                     } else {
                         System.out.println("There was a problem with the deletion.");
                     }
                     break;
                }else {
                	System.out.println("This file could not be found.");
                	break;
                }
            } catch (RemoteException e) {
                //System.out.println("Something went wrong, retrying... " + retries);
                if (--retries <= 0) System.err.println(e.getMessage());
            }
            retries = 10;
        }
    }
    
    //Manage the client requests
    private static void requestManager() {
    	int option;
        Scanner keyInput = new Scanner(System.in);
        String filename;
        
    	do {
            System.out.println("Choose an option and type its number." );
            System.out.println("1) Retrieve the list of uploaded files.");
            System.out.println("2) Download a file.");
            System.out.println("3) Upload a file.");
            System.out.println("4) Delete a file.");
            System.out.println("0) Exit.");
            option = keyInput.nextInt();

            switch (option) {
                case 1:
                	showFileList();
                    break;
                case 2:
                    System.out.println("Which file do you wish to download?");
                    keyInput.nextLine(); //consume any "\n"
                    filename = keyInput.nextLine();
                    downloadFile(filename);
                    break;
                case 3:
                    System.out.println("Which file do you wish to upload?");
                    keyInput.nextLine(); //consume any "\n"
                    filename = keyInput.nextLine();
                    uploadFile(filename);
                    break;
                case 4:
                    System.out.println("Which file do you wish to delete?");
                    keyInput.nextLine(); //consume any "\n"
                    filename = keyInput.nextLine();
                    deleteFile(filename);
                    break;
                default:
                    System.out.println("Client exited..");
                    break;
            }
        }while(option != 0);

        keyInput.close();
    }
}
