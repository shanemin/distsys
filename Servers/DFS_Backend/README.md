##DFS Backend Readme
___
###Changelog - 16/12/18

####Functionality Changes

* Load balancing now uses tree topology and message propagation from children to parent, once their load is updated. Eventually the message reaches the master.
    * Assumes 0 load at the beginning. Doesn't read folder sizes.
    * On upload/delete the node calculates its new load and decides on which of himlself and the candidates propagated by the children has the smallest load. In equal loads between parent and candidate, the parent decides itself, so that if it is picked by the master, the notification message after executing the operation and updating the load will reach the master faster.
    * Master chooses the node with the least load, as given by its children, until new updates to this information reach it (eventually updated)
    * Tested with 4 nodes - 4 files. Worked.
    * Nodes that store/delete replicas also notify parent

####File Changes

1. Removed Heartbeat messages for testing purposes. Uncomment to reuse.
2. Declared a *notifyOnLoadChange* method in *Connector_Intf* inside the *general* package. Master and slave implement it by their own needs in *MasterConnector* and *SlaveConnector* respectively. This is an RMI method called by children to notify their parents about who they have decided that it has the least load between themselves and the candidates send by their children with *notifyOnLoadChange*. Initially the servers consider themselves as the candidate and once a child is added, they switch to condsidering it as the new candidate. This switch happens inside the *connect* method of the same interface. Of course this decision holds only at the beginning when the conenctions are created and changes when files are uploaded/deleted.
3. Created an extra MasterNode class as an extension to ServerNode, because the master needs some things that the slaves do not. This is were the full list of all connections (servers) is located now.
4. ServerInfo class now stores the load of the current node and provides functions to add or remove load. It also stores the node that it considers it has the least load. This is set in other classes depending on decisions after calls to the *notifyOnLoadChange* in (2).
5. ServerNode now has 2 functions related to load balancing decisions. One is *decideOnLeastLoad*, which is used after the *notifyOnLoadChange* in (2) is called and updates the node's decision on who's the candidate with the least node between itself and the candidates propagated by its children. The other function, *notifyParent()* simply notifies the parent about this decision and it's called inside *decideOnLeastLoad* only when needed. 
6. There are two cases where uploads and downloads occur. During handling these operations for the client inside *ClientRedirectedRequests* of the *general/clientUtils* package and when copying/removing replicas, inside the *general/replicationServices* package, by *Replicaror*. Both these classes take care of updating the current server's load and notifying the parent, when these operations  occur. Also in *ClientRequests* (which is used only by the master to manage client requests-redirects) of the *general/clientUtils* package, the *uploadRedirect* function now sends back to the client the decision of the master instead of random choise.
7. Renamed *SlaveConnectorBinTree* and *SlaveConnectorBinTree_Intf* to *SlaveConnector* and *SlaveConnector_Intf* in the *slave_node* package.
8. Renamed *TreeNetwork* to *TopologyConstructor* in the *master_node* package

####Execute - Notes
* Remember to delete any files from servers/database to be able to see the new changes. I think in a previous commit some files were uploaded as well.
* use *maven install* . It will create a jar with all the dependencies needed.
* run the *jar-with-dependencies* by *java -jar 'filename' 'arguments'*
    * Do not specify the class, it's contained in the MANIFEST file
    * Remember to open mysql first (server or whatever you need)
    * Have the *Resources* folder at the same place as the jar, because it will look for it.
    * Unless a connection happens, the servers don't display anything after the initial message.

###1. General =====================================================
The project is built with **Maven** configured for **Java 1.8**.

This code has the implementation of the server as either a master or a slave node. To select which mode pass the following arguments to the command line:

* master : 
    * this server's rmi registry port
* slave : 
    * this server's rmi registry port 
    * master's ip address
    * master's registry port

The registry for rmi will be created inside the code, there's no need to start it manually.

Every Server, regradles of mode, creates a (or uses existing) folder *"Client_Files/S_(ip)_(port)"*. For example the server at *localhost : 3000* will create the folder *"Client_Files/S_localhost_3000"*. Inside this folder, for each user, another folder will be created named *"User(user id)"*. So the files of User1 will be in the previous case inside *"Client_Files/S_localhost_3000/User1"*.

Interfaces end with *"_Intf"* to make them visually distinguishable. The objects that implement those have the same name, apart from the *"_Intf"* suffix.

###2. Code Explanation

####Main Class : /src/main/java/jarg/networking/dfs/DFS_Backend/DFS_Server.java

The *main* method is in *DFS_Server.java*. After retrieving information from the argument list the server will operate as either master or slave.

Here, the *security.policy* file is loaded. This file is required to allow transfering serialized objects with RMI. Currently the file is placed in *src/main/resources*, which is the default folder for resources for *Maven*. A secutiry manager must also be set for this to work. This happens at the top of the *main* method. 

If the server is a master node, the it just creates an RMI registry and binds the RMI object that "listens" for other server connections to it.

If it is a slave node, it first creates an RMI registry and binds to it an object that listens to connections from children. Then, it connects to the master and afterwards to its parent in the tree topology, as returned after the connection to the master. Once this is finished, it registers to its own registry an RMI object that handles client requests.

####Package : src/main/java/jarg/networking/dfs/DFS_Backend/general

All code that will de used in both types of server is in this package.

* **DefaultValues** : it holds constants.
* **ServerInfo** : describes minimal information about a server like its IP, Port and if it's a master node or not. It's used by a server to describe its children, siblings, etc.
* **ServerNode** : **extends ServerInfo** by holding more information, like the children, the siblings, etc. It's usually instanced to describe the current running server. 
* **Connector_Intf** : an interface which defines the functions used by all servers to allow other servers to connect with them through RMI. It is implemented differently by master and slave nodes.
* **SlaveConnectorBinTree_Intf** : **extends Connector_Intf** and adds a function that allows parents to call this on their children, to pass them their siblings that will replicate their files. This function is called by a parent to a child. It's made specifically to pass only one sibling, since it assumes a binary tree topology.

####Package : src/main/java/jarg/networking/dfs/DFS_Backend/general/replicationServices

This package contains functionality that implements replication of files between 2 sibling nodes.

* **Repilcator_Intf** : the interface to define the remote object's functions that implement replication.
* **Replicator** : the implementation of the previous interface. It is used by a server when a client uploads a file to it, to send that file to its sibling as well.
* **ReplicationService** : runs a thread to use a **Replicator** remote object to send a copy of a file after the client ulploaded it. It runs on the background, while the current server stores that file.
* **ReplicationService** : same as above, but for deleting a file.

####Package : src/main/java/jarg/networking/dfs/DFS_Backend/general/clientUtils

This package contains the functionality related to client operations like donwload, upload, etc.

* **Document** : describes a user's file that will be uploaded, downloaded, etc. It packs the file's data along with relevant information, like user ID, etc.
* **ClientUtils_Intf** : defines the operations that the client can execute.
* **ClientUtils** : implements the previous interface. It also creates any folders that are needed but don't exist. In the upload and delete functions, it uses the functionality of the previous package to manage the replica files.

####Package : src/main/java/jarg/networking/dfs/DFS_Backend/master_node

This package contains functionality required only by the master node.

* **ConnectionsInfo** : it's used to hold a list of all the servers connected to the master (In a next version it will be moved inside an extension of ServerNode) .
* **MasterConnector** : implements the **Connector_Intf** of *"src/main/java/jarg/networking/dfs/DFS_Backend/general"*. When a server connects, the master retrieves to it its parent. If that node happens to be a direct child of the master, it passes it its siblings too.
* **TopologyConstructor_Intf** : an interface defining a function that will return the parent of a server in an undefined network topology.
* **TreeNetwork** : **implements TopologyConstructor_Intf** to retrieve the parent in a hierarchical tree topology, based on the number of children that a node will have.


####Package : src/main/java/jarg/networking/dfs/DFS_Backend/slave_node

This package has functionality required only by slave nodes.

* **SlaveConnectorBinTree** : **implements SlaveConnectorBinTree_Intf** of the package *"src/main/java/jarg/networking/dfs/DFS_Backend/general"*. Allows children to connect to this server and also allows the parent of this server to send to it its siblings.

