CREATE SCHEMA IF NOT EXISTS `DFS_db` ;
USE `DFS_db`;
CREATE TABLE IF NOT EXISTS `DFS_db`.`users` (
  `_id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NULL,
  `password` VARCHAR(45) NULL,
  PRIMARY KEY (`_id`));
CREATE TABLE IF NOT EXISTS `DFS_db`.`files` (
  `_id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `filename` VARCHAR(45) NULL,
  `size` BIGINT NOT NULL,
  `server_1_address` VARCHAR(45) NULL,
  `server_2_address` VARCHAR(45) NULL,
  PRIMARY KEY (`_id`),
  INDEX `_id_idx` (`user_id` ASC),
  CONSTRAINT `_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `DFS_db`.`users` (`_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);
