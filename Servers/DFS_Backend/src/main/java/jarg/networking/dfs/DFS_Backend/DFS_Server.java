/*
 * The application's entry point. 
 * It starts up a server in either master or slave mode, depending on 
 * arguments passed.
 */
package jarg.networking.dfs.DFS_Backend;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import jarg.networking.dfs.DFS_Backend.faultTolerance.Heartbeat;
import jarg.networking.dfs.DFS_Backend.faultTolerance.Heartbeat_Intf;
import jarg.networking.dfs.DFS_Backend.general.Connector_Intf;
import jarg.networking.dfs.DFS_Backend.general.DefaultValues;
import jarg.networking.dfs.DFS_Backend.general.MasterNode;
import jarg.networking.dfs.DFS_Backend.general.ServerInfo;
import jarg.networking.dfs.DFS_Backend.general.ServerNode;
import jarg.networking.dfs.DFS_Backend.general.clientUtils.ClientRedirectedRequests;
import jarg.networking.dfs.DFS_Backend.general.clientUtils.ClientRedirectedRequests_Intf;
import jarg.networking.dfs.DFS_Backend.general.clientUtils.ClientRequests;
import jarg.networking.dfs.DFS_Backend.general.clientUtils.ClientRequests_Intf;
import jarg.networking.dfs.DFS_Backend.general.database.DatabaseManager;
import jarg.networking.dfs.DFS_Backend.general.replicationServices.Replicator;
import jarg.networking.dfs.DFS_Backend.general.replicationServices.Replicator_Intf;
import jarg.networking.dfs.DFS_Backend.master_node.MasterConnector;
import jarg.networking.dfs.DFS_Backend.slave_node.SlaveConnector;
import jarg.networking.dfs.DFS_Backend.slave_node.SlaveConnector_Intf;
import java.util.Iterator;

public class DFS_Server implements Runnable {

    private final int HEARTBEAT_FREQUENCY = 2000;
    private static ServerInfo parent_info;
    private static MasterNode master_instance;
    private static ServerNode server_instance;
    private static DatabaseManager db_manager; //this will handle all database operations
    private static boolean is_master = false;
    private static int master_port;
    private static String master_ip;
    //Rmi stubs and registries moved here to keep the references alive and prevent being Garbage Collected in main() and stopping the server after a while
    private static Connector_Intf master_con;
    private static Connector_Intf master_con_rmo;
    private static Heartbeat_Intf hb;
    private static Heartbeat_Intf hb_rmo;
    private static ClientRequests_Intf cl_reqs;
    private static ClientRequests_Intf cl_reqs_rmo;
    private static SlaveConnector_Intf slave_con;
    private static SlaveConnector_Intf slave_con_rmo;
    private static Registry master_registry;
    private static Connector_Intf master_connector;
    private static  Registry parent_registry;
    private static SlaveConnector_Intf parent_connector;
    private static ClientRedirectedRequests_Intf client_utils_rmo;
    private static ClientRedirectedRequests_Intf client_utils;
    private static Replicator_Intf repl_utils;
    private static  Replicator_Intf repl_rmo;
    
    //Initialize to null. Later only the one used will be assigned to a new object
    static {
        master_instance = null;
        server_instance = null;
    }

    public static void main(String[] args) {
        //Use security policy to be allowed to transfer objects with RMI
        System.setProperty("java.security.policy", DefaultValues.RESOURCES_FOLDER + "security.policy");
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }

        //Start your service
        switch (args.length) {
            case 1: {
                //Master Code ==========================
                //Create node instance
                is_master = true;
                System.out.println("Server started in master mode, at port " + args[0]);
                int server_port = Integer.parseInt(args[0]);	//the port of this server
                master_instance = new MasterNode("localhost", server_port, true);
                master_port = Integer.parseInt(args[0]);
                //Initialize Database Connection
                db_manager = new DatabaseManager(master_instance);

                //Register remote object for connections of servers to master
                master_con = new MasterConnector(master_instance);
                hb = new Heartbeat();
                cl_reqs = new ClientRequests(master_instance, db_manager);

                try {
                    master_con_rmo = (Connector_Intf) UnicastRemoteObject.exportObject(master_con, 0);
                    master_instance.createRegistry();
                    master_instance.getRegistry().bind("Master_Connector", master_con_rmo);
                    hb_rmo = (Heartbeat_Intf) UnicastRemoteObject.exportObject(hb, 0);
                    master_instance.getRegistry().bind("Heartbeat", hb_rmo);
                    cl_reqs_rmo = (ClientRequests_Intf) UnicastRemoteObject.exportObject(cl_reqs, 0);
                    master_instance.getRegistry().bind("CL_Requests_Main", cl_reqs_rmo);
                } catch (RemoteException | AlreadyBoundException e) {
                    System.err.println(e.getMessage());
                }

                //Start server Hearbeat 
                new DFS_Server().run();

                break;
            }
            case 3: {
                //Slave code ==============================
                System.out.println("Server started in slave mode, at port " + args[0]);
                int server_port = Integer.parseInt(args[0]);	//the port of this server
                master_ip = args[1];                     //the ip of the master
                master_port = Integer.parseInt(args[2]);	//the port of the master node
                server_instance = new ServerNode("localhost", server_port, false);
                //Initialize Database Connection
                db_manager = new DatabaseManager(server_instance);

                //Open your own registry for your children
                slave_con = new SlaveConnector(server_instance, db_manager);

                try {
                    slave_con_rmo = (SlaveConnector_Intf) UnicastRemoteObject.exportObject(slave_con, 0);
                    server_instance.createRegistry();
                    server_instance.getRegistry().bind("Child_Connector", slave_con_rmo);
                } catch (RemoteException | AlreadyBoundException e) {
                    System.err.println(e.getMessage());
                }

                //Connect to master node and get your parent
                parent_info = null;
              
                try {
                    master_registry = LocateRegistry.getRegistry(master_ip, master_port);
                    master_connector = (Connector_Intf) master_registry.lookup("Master_Connector");
                    parent_info = master_connector.connect(server_port);
                    server_instance.setParent(parent_info);
                } catch (RemoteException | NotBoundException e) {
                    System.err.println(e.getMessage());
                }

                //Connect to your parent
                parent_connector = null;

                if ((!master_ip.equals(parent_info.getIp())) || (master_port != parent_info.getPort())) {
                    try {
                        parent_registry = LocateRegistry.getRegistry(parent_info.getIp(), parent_info.getPort());
                        parent_connector = (SlaveConnector_Intf) parent_registry.lookup("Child_Connector");
                        parent_connector.connect(server_port);
                    } catch (RemoteException | NotBoundException e) {
                        System.err.println(e.getMessage());
                    }
                }

                //Does my sibling have any files that I don't yet?
                if(server_instance.getSiblings().size() > 0) {
                	server_instance.copySiblingFiles(server_instance.getSiblings().get(0), db_manager);
                }
                
                //Register your remote objects 
                client_utils = new ClientRedirectedRequests(server_instance, db_manager);
                repl_utils = new Replicator(server_instance, db_manager);
                hb = new Heartbeat();
                
                try {
                    client_utils_rmo = (ClientRedirectedRequests_Intf) UnicastRemoteObject.exportObject(client_utils, 0);
                    server_instance.getRegistry().bind("CL_Requests_Redirected", client_utils_rmo);
                    repl_rmo = (Replicator_Intf) UnicastRemoteObject.exportObject(repl_utils, 0);
                    server_instance.getRegistry().bind("Replicator", repl_rmo);
                    hb_rmo = (Heartbeat_Intf) UnicastRemoteObject.exportObject(hb, 0);
                    server_instance.getRegistry().bind("Heartbeat", hb_rmo);
                } catch (RemoteException | AlreadyBoundException e) {
                    System.err.println(e.getMessage());
                }

                break;
            }
            default:
                System.out.println("Insufficient Arguments. "
                        + "Expecting : <server registry port> "
                        + "and additionally in slave mode : "
                        + "<master's registry ip> <master's registry port>");
                break;
        }
    }

    @Override
    @SuppressWarnings("SleepWhileInLoop")
    public void run() {
        Heartbeat_Intf hb_connector = null;

            while (true) {
                try {
                    Thread.sleep(HEARTBEAT_FREQUENCY);

                    for (ServerInfo node : master_instance.getConnections()) {
                        if (node.isMaster()) {
                            continue;
                        }

                        try {
                            if (node.getStatus()) {
                                Registry child_registry = LocateRegistry.getRegistry(node.getIp(), node.getPort());
                                hb_connector = (Heartbeat_Intf) child_registry.lookup("Heartbeat");
                                hb_connector.startTimer();
                                hb_connector.receiveHeartbeat("HB received ");
                                //System.out.print(node.getPort() + "(" + tree_index + ") ");
                            } else {
                                //System.out.print(node.getPort() + "(d/c)");
                            }
                        } catch (NotBoundException | RemoteException ex) {
                            node.setStatus(false);
                            System.out.println("Disconnected:\t Slave node <" + node.getIp() + " , " + node.getPort() + ">.");
                        }
                    } //System.out.println();
                } catch (InterruptedException ex) {
                    System.out.println(ex);
                }
            }
        }
}
