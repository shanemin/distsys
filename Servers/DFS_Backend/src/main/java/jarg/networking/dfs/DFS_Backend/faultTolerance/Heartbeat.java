package jarg.networking.dfs.DFS_Backend.faultTolerance;

import java.rmi.RemoteException;
import java.util.Timer;
import java.util.TimerTask;

public class Heartbeat implements Heartbeat_Intf {

    public Timer timer;
    public int timerRetries = 20;
    public String ip;
    public int port;

    class CheckTimeout extends TimerTask {

        @Override
        public void run() {
            if (timerRetries == 0) {
                System.out.println("TIMEOUT!");
                timer.cancel();
            } else {
                try {
                    timerRetries--;
                    System.out.printf("Missed heartbeat from %s %d, retries left: %d\n", ip, port, timerRetries);
                    timer.cancel();
                    startTimer();
                } catch (RemoteException ex) {
                    System.out.println(ex);
                }
            }
        }
    }

    @Override
    public void setServerDetails(String ip, int port) throws RemoteException {
        this.ip = ip;
        this.port = port;
    }

    @Override
    public void startTimer() throws RemoteException {
        timer = new Timer();
        timer.schedule(new CheckTimeout(), 1000);
    }

    @Override
    public synchronized void receiveHeartbeat(String msg) throws RemoteException {
        //System.out.println(msg);
        timerRetries = 20;
        timer.cancel();
    }

}
