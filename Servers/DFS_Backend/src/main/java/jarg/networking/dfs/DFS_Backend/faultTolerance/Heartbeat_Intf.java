package jarg.networking.dfs.DFS_Backend.faultTolerance;

import java.rmi.*;

public interface Heartbeat_Intf extends Remote {
    
    public void startTimer() throws RemoteException;
    
    public void setServerDetails(String ip, int port) throws RemoteException;
    
    public void receiveHeartbeat(String msg) throws RemoteException;

}
