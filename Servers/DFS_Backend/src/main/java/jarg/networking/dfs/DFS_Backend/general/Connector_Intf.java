/*
 * This is the interface of the remote object that this server will use, to allow
 * other servers to connect to it. Master and Slave nodes implement it in a different manner,
 * according to their needs.
 */
package jarg.networking.dfs.DFS_Backend.general;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface Connector_Intf extends Remote {
    //This function will be called by any server that wishes to connect to this one.
    //The server passes which port does its own registry listen to.
    public ServerInfo connect(int slave_registry_port) throws RemoteException;
    //This function will be used by a server to notify its parent when a change in its load occurs
    public void notifyOnLoadChange(int caller_port, ServerInfo candidate) throws RemoteException;
    
    public ArrayList<ServerInfo> getServerList() throws RemoteException;
}
