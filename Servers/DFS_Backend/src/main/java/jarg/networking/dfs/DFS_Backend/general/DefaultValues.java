/*
 * This class will contain constants used throughout the program
 */
package jarg.networking.dfs.DFS_Backend.general;

import java.io.File;

public final class DefaultValues {
	public static final int MAX_CHILDREN = 2;
	public static final String RESOURCES_FOLDER = "Resources" + File.separator;
	public static String DEFAULT_FOLDER = "Client_Files"+File.separator;
	//public static final DefaultValues instance = new DefaultValues();
	
	private DefaultValues() {
		// TODO Auto-generated constructor stub
	}

}
