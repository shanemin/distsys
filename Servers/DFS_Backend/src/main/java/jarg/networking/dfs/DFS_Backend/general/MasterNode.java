/*
 * Extends the ServerNode class with more features, to be used only by the master node
 */
package jarg.networking.dfs.DFS_Backend.general;

import java.util.ArrayList;

public class MasterNode extends ServerNode {
	 private ArrayList<ServerInfo> connections;	//To be used only by the Master node - The list of all the servers including itself in position 0.
	
	public MasterNode(String ip, int port, boolean master_mode) {
		super(ip, port, master_mode);
		connections = new ArrayList<>();
	}

	public synchronized ArrayList<ServerInfo> getConnections() {
		return connections;
	}

	public synchronized void setConnections(ArrayList<ServerInfo> connections) {
		this.connections = connections;
	}
}
