/*
 * This class will be used to describe a server and its features
 * in RMI messages
 */
package jarg.networking.dfs.DFS_Backend.general;

import java.io.Serializable;

public class ServerInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    private String ip;
    private int port;
    private boolean master_mode;
    private ServerInfo parent;
    private long load;							//The size of all user files it has stored  -  the load	
    private ServerInfo leastLoadNode;					//Will be either itself or a child, depending on who has the least load
    private boolean isAlive;

    public ServerInfo(String ip, int port, boolean master_mode) {
        this.ip = ip;
        this.port = port;
        this.master_mode = master_mode;
        setLoad(0);
        setLeastLoadNode(this);
        isAlive = true;
    }

    public void setStatus(boolean status) {
        isAlive = status;
    }

    public boolean getStatus() {
        return isAlive;
    }

    public ServerInfo getParent() {
        return parent;
    }

    public void setParent(ServerInfo parent) {
        this.parent = parent;
    }

    public boolean isMaster() {
        return master_mode;
    }

    public void setMaster_mode(boolean master_mode) {
        this.master_mode = master_mode;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    //To be called to identify a node in a list of nodes. If two objects have the same ip and port, they represent the same server
    public boolean isIdentical(ServerInfo another_server) {
        return (this.getIp().equals(another_server.getIp())) && (this.getPort() == another_server.getPort());
    }

    //To be called to identify a node in a list of nodes. If two objects have the same ip and port, they represent the same server
    public boolean isIdentical(String ip, int port) {
        boolean ip_equals;
        //Need to check this as well
        if (ip.equals("127.0.0.1") && this.getIp().equals("localhost")) {
            ip_equals = true;
        } else if (ip.equals("localhost") && this.getIp().equals("127.0.0.1")) {
            ip_equals = true;
        } else if (ip.equals(this.getIp())) {
            ip_equals = true;
        } else {
            ip_equals = false;
        }
        return ip_equals && (this.getPort() == port);
    }

    public long getLoad() {
        return load;
    }

    public void setLoad(long load) {
        this.load = load;
    }

    //To be called when a new file is uploaded to the server
    public void addLoad(long new_load) {
        this.load += new_load;
        System.out.println("Added load " + new_load + ". New total load : " + this.load);
    }

    //To be called when a file is deleted from the server
    public void subtractLoad(long new_load) {
        this.load -= new_load;
        System.out.println("Subtracted load " + new_load + ". New total load : " + this.load);
    }

    public ServerInfo getLeastLoadNode() {
        return leastLoadNode;
    }

    //To be called when this server decides who has the minimum load
    public void setLeastLoadNode(ServerInfo leastLoadNode) {
        this.leastLoadNode = leastLoadNode;
    }

    @Override
    public String toString() {
    	if(this.getIp().equals("127.0.0.1")) {
    		return "localhost" + ":" + this.getPort();
    	}
        return this.getIp() + ":" + this.getPort();
    }
}
