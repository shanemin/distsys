/*
 * This is a class describing the current server running, regardless of being a master or slave node.
 * Instances of it should not be sent through RMI requests. ServerInfo instances should be used for 
 * that need instead.
 */
package jarg.networking.dfs.DFS_Backend.general;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import jarg.networking.dfs.DFS_Backend.general.clientUtils.ClientRedirectedRequests;
import jarg.networking.dfs.DFS_Backend.general.clientUtils.ClientRedirectedRequests_Intf;
import jarg.networking.dfs.DFS_Backend.general.clientUtils.Document;
import jarg.networking.dfs.DFS_Backend.general.database.DatabaseManager;

public class ServerNode extends ServerInfo {

    private ArrayList<ServerInfo> children;			//The children in the tree topology
    private ArrayList<ServerInfo> siblings;			//The siblings (same level nodes with the same parent) in the tree topology
    private Registry registry;					//The RMI registry created by an instance
    private boolean isAlive;
    private String serverFolder;

    public ServerNode(String ip, int port, boolean master_mode) {
        super(ip, port, master_mode);
        children = new ArrayList<>();
        setSiblings(new ArrayList<>());
        registry = null;
        isAlive = true;
        setServerFolder(DefaultValues.DEFAULT_FOLDER +  "S_"+this.getIp()+"_"+this.getPort()+File.separator);
    }

    public synchronized ArrayList<ServerInfo> getChildren() {
        return children;
    }

    public synchronized void setChildren(ArrayList<ServerInfo> nodes) {
        children = nodes;
    }

    public synchronized ArrayList<ServerInfo> getSiblings() {
        return siblings;
    }

    public synchronized void setSiblings(ArrayList<ServerInfo> siblings) {
        this.siblings = siblings;
    }

    public synchronized void addChild(ServerInfo child) {
        children.add(child);
    }

    public synchronized void createRegistry() {
        try {
            registry = LocateRegistry.createRegistry(getPort());
        } catch (RemoteException e) {
            System.err.println(e.getMessage());
        }
    }

    public synchronized Registry getRegistry() {
        return registry;
    }

    //Call to notify the parent when the node with the least load has changed
    public void notifyParent() {
        System.out.println("Sending notification to parent");
        Registry parent_reg;
        Connector_Intf parent_obj;
        try {
            parent_reg = LocateRegistry.getRegistry(this.getParent().getIp(), this.getParent().getPort());
            if (this.getParent().isMaster()) {
                parent_obj = (Connector_Intf) parent_reg.lookup("Master_Connector");
            } else {
                parent_obj = (Connector_Intf) parent_reg.lookup("Child_Connector");
            }
            parent_obj.notifyOnLoadChange(this.getPort(), this.getLeastLoadNode());
        } catch (RemoteException | NotBoundException e) {
            System.err.println("Error in notifying parent. " + e.getMessage());
        }
    }

    //Decide on the node with the least load when an update happens
    public void decideOnLeastLoad() {
        ServerInfo previousLeastLoadNode = this.getLeastLoadNode();	//keep for later
        //Find the server with the minimum load - You have to check both your children and yourself
        //Is it a leaf node?
        if (this.getChildren().size() == 0) {
            this.setLeastLoadNode(this);
            System.out.println("No children, so this remains the server with the least load");
            this.notifyParent();	//propagate the new results to the parent
        } else {
            if (!this.isMaster()) { //if you're not the master
                this.setLeastLoadNode(this);
                for (ServerInfo child : this.getChildren()) {
                    if (child.getLeastLoadNode().getLoad() < this.getLeastLoadNode().getLoad()) {
                        this.setLeastLoadNode(child.getLeastLoadNode());
                    }
                }
            } else { //if you are the master
                this.setLeastLoadNode(this.getChildren().get(0).getLeastLoadNode());
                for (int i = 1; i < this.getChildren().size(); i++) {
                    if (this.getChildren().get(i).getLeastLoadNode().getLoad() < getLeastLoadNode().getLoad()) {
                        this.setLeastLoadNode(this.getChildren().get(i).getLeastLoadNode());
                    }
                }
            }
            System.out.println("Server with min load : " + this.getLeastLoadNode().toString());
            //If the node with the least node changed, notify the parent
            if (!(this.isMaster()) && !(previousLeastLoadNode.isIdentical(this.getLeastLoadNode()))) {
                this.notifyParent();	//propagate the new results to the parent
            }
        }
    }

	public String getServerFolder() {
		return serverFolder;
	}

	public void setServerFolder(String serverFolder) {
		this.serverFolder = serverFolder;
	}
	
	//Use this function to get a copy of the sibling's files
	public void copySiblingFiles(ServerInfo sibling, DatabaseManager db_manager) {
		 Map<Integer, ArrayList<String>> files = db_manager.getAllFilesPerUserForServer(sibling.toString()); 
		 Registry siblingRegistry;
		 ClientRedirectedRequests_Intf copy_stub;
         try {
			 if(!files.isEmpty()) { //now I have to get those files too before moving on
	         	siblingRegistry = LocateRegistry.getRegistry(sibling.getIp(), sibling.getPort());
	         	//Act like a client to download files
	         	copy_stub = (ClientRedirectedRequests_Intf) siblingRegistry.lookup("CL_Requests_Redirected");
	         	Document doc;
	         	String filepath = "";
	         	//For every file in that list, request it from the sibling and write it to your storage
	         	for(Entry<Integer, ArrayList<String>> entr : files.entrySet()) {
	         		for(String file : entr.getValue()) {
	         			doc = copy_stub.donwloadFile(entr.getKey(), file);
	         			String directory = this.getServerFolder() + "User"+ String.valueOf(entr.getKey()) + File.separator;
	         			Files.createDirectories(Paths.get(directory));
	         			filepath =  directory + doc.getFilename();
	         			try(FileOutputStream fileout = new FileOutputStream(filepath)){
	         				fileout.write(doc.getData());
	         			}catch(IOException e) {
	         				System.err.println(e.getMessage());
	         			}
	         		}
	         	}
	         	System.out.println("Finished getting sibling's files");
			 }
         }catch(NotBoundException | IOException e) {
        	 System.err.println(e.getMessage());
         }
	}

}
