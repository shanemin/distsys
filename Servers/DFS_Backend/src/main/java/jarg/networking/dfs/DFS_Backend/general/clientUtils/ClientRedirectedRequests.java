package jarg.networking.dfs.DFS_Backend.general.clientUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.RemoteException;

import jarg.networking.dfs.DFS_Backend.general.ServerInfo;
import jarg.networking.dfs.DFS_Backend.general.ServerNode;
import jarg.networking.dfs.DFS_Backend.general.DefaultValues;
import jarg.networking.dfs.DFS_Backend.general.database.DatabaseManager;
import jarg.networking.dfs.DFS_Backend.general.replicationServices.DelReplicationService;
import jarg.networking.dfs.DFS_Backend.general.replicationServices.ReplicationService;

public class ClientRedirectedRequests implements ClientRedirectedRequests_Intf {
	private ServerNode server_instance;
	private DatabaseManager db_manager; //this will handle all database operations
	
	public ClientRedirectedRequests(ServerNode server_instance, DatabaseManager db_manager) {
		this.server_instance = server_instance;
		this.db_manager = db_manager;
		try {
			Files.createDirectories(Paths.get(server_instance.getServerFolder()));
		}catch( FileAlreadyExistsException e) {
			//No need to print anything
		}catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	//With this, the server create any directories needed for the user in its storage space
	private void initializeUserDirectory(int user_id){
		try {
			Files.createDirectories(Paths.get(server_instance.getServerFolder() + "User"+ String.valueOf(user_id)));
		}catch( FileAlreadyExistsException e) {
			//No need to print anything
		}catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	//Download a file
	@Override
	public synchronized Document donwloadFile(int user_id, String filename) throws RemoteException {
		String filepath = server_instance.getServerFolder() + "User"+ String.valueOf(user_id)+ File.separator + filename;
		Path path = Paths.get(filepath);
		Document doc;
		if(Files.exists(path)) {
			doc = new Document(user_id, filename, path, true);
		}else {
			return null;
		}
		return doc;
	}

	//Upload a new file
	@Override
	public synchronized boolean uploadFile(int user_id, Document doc) throws RemoteException { 
		boolean success = false;
		
		if(doc != null) {
			//create user directory if it doesn't exist
			initializeUserDirectory(user_id);
			String file_path = server_instance.getServerFolder() + "User"+ String.valueOf(user_id)+ File.separator + doc.getFilename();
			//Check if file already exists
			Path check_path = Paths.get(file_path);
			boolean already_exists = Files.exists(check_path);
			long previous_size = 0;
			if(already_exists) {
				try {
					previous_size = Files.size(check_path);
				} catch (IOException e) {
					System.err.println("Error in obtaining previous file size");
				}
			}
			try(FileOutputStream out_file = new FileOutputStream(file_path)){
    			//Write the file to your storage
    			out_file.write(doc.getData());
    			//Update information
    			if(!already_exists) { //update this info only if it's a new file
    				//Also update the database's information
        			db_manager.storeUploadedFileInfo(user_id, doc.getFilename(), doc.getData().length);
    				//update your status
        			server_instance.addLoad(doc.getData().length);
        			//notify parent of load change if needed
        			server_instance.decideOnLeastLoad();
    			}else {
    				long new_size = doc.getData().length;
    				long difference = new_size - previous_size;
    				if(difference == 0) {
    					System.out.println("The new version of the file was stored. The load remains the same.");
    				}else {
    					System.out.println("The new version of the file was stored.");
    					db_manager.updateFileSize(user_id, doc.getFilename(), new_size);
    					//update your status
            			server_instance.addLoad(difference);
            			//notify parent of load change if needed
            			server_instance.decideOnLeastLoad();
    				}
    			}
    			//Replicate the file in the background - This HAS to happen last, otherwise the database might not be updated correctly because this uses threads
    			replicateFile(user_id, doc, server_instance.getSiblings().get(0));
    			success = true;
    		}catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}else {
			System.out.println("Invalid upload request : empty file");
		}
		return success;
	}
	
	@Override
	public synchronized boolean deleteFile(int user_id, String filename) throws RemoteException {
		boolean success = false;
		String filepath = server_instance.getServerFolder() + "User"+ String.valueOf(user_id)+ File.separator + filename;
		Path path = Paths.get(filepath);
		if(Files.exists(path)) {
			try {
				//Delete your own copy
				long file_size = Files.size(path);
				Files.delete(path);
				//Also delete the database's information
				db_manager.deleteFileInfo(user_id, filename);
				//Have the server that has the replica delete their copy
				deleteReplicateFile(user_id, filename, server_instance.getSiblings().get(0));
				//update your status
    			server_instance.subtractLoad(file_size);
    			//notify parent of load change if needed
    			server_instance.decideOnLeastLoad();
    			success = true;
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}
		return success;
	}
	
	//Replica methods ***************************************************************************
	
	//Send the file to the sibling
	public void replicateFile(int user_id, Document doc, ServerInfo replica_server) {
		Thread repl_service = new Thread(new ReplicationService(user_id, doc, replica_server));
		repl_service.start();
	}
	
	
	//Delete file from sibling
	public void deleteReplicateFile(int user_id, String filename, ServerInfo replica_server) {
		Thread del_repl_service = new Thread(new DelReplicationService(user_id, filename, replica_server));
		del_repl_service.start();
	}


	


}
