package jarg.networking.dfs.DFS_Backend.general.clientUtils;

import java.rmi.Remote;

/*
 * This interface defines methods for communication of clients directly with slave nodes
 */

import java.rmi.RemoteException;
import java.util.ArrayList;

public interface ClientRedirectedRequests_Intf extends Remote  {
	public Document donwloadFile(int user_id, String filename) throws RemoteException;	//Download a file
	public boolean uploadFile(int user_id, Document doc) throws RemoteException;			//Upload a file. Returns false or error or true otherwise
	public boolean deleteFile(int userID, String filename) throws RemoteException;			//Delete a file. Returns false or error or true otherwise
}
