package jarg.networking.dfs.DFS_Backend.general.clientUtils;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Random;

import jarg.networking.dfs.DFS_Backend.general.ServerInfo;
import jarg.networking.dfs.DFS_Backend.general.MasterNode;
import jarg.networking.dfs.DFS_Backend.general.database.DatabaseManager;

public class ClientRequests implements ClientRequests_Intf {
	private MasterNode master_instance;
	private DatabaseManager db_manager; //this will handle all database operations
	
	public ClientRequests(MasterNode master_instance, DatabaseManager db_manager) {
		this.master_instance = master_instance;
		this.db_manager = db_manager;
	}

	//Master only *******************************************************************************
	
	//Client creates account
	@Override
	public int createAccount(String username, String password) throws RemoteException{
		return db_manager.createAccount(username, password);
	}
	
	//Clients logs in with username and password and gets an id back
	@Override
	public int logIn(String username, String password) throws RemoteException{
		return db_manager.userLogIn(username, password);
	}
	
	//The master will use this to redirect the client to correct server.
	//This one is used to retrieve which servers have a file.
	@Override
	public String donwloadRedirect(int userID, String filename) throws RemoteException{
		return db_manager.getServerForFile(userID, filename);
	}

	//Random load balancing for now
	@Override
	public String uploadRedirect(int user_id, String filename) throws RemoteException {
// 		//Random Solution
//		Random rand = new Random();
//		ArrayList<ServerInfo> servers = master_instance.getConnections();
//		int server_index = rand.nextInt(servers.size());
//		if(server_index == 0) {
//			server_index ++;
//		}
//		return servers.get(server_index).toString();
		
		//Check if file already exists
		String server = db_manager.getServerForFile(user_id, filename);
		if(server.equals("")) {
			server = master_instance.getLeastLoadNode().toString();
		}
		//Return the node with the least load
		return  server;	
	}
	
	//Used by the client to ask a master where to issue a delete request
	@Override
	public String deleteRedirect(int user_id, String filename) throws RemoteException {
		return db_manager.getServerForFile(user_id, filename);
	}
	
//	
//	//Retrieve a user's list of files by looking at storage (No Database)
//	@Override
//	public synchronized String retrieveFileList(int user_id) throws RemoteException {
//		String files = "";
//		Path dir = Paths.get(DEFAULT_FOLDER + "User"+ String.valueOf(user_id));
//		try(DirectoryStream<Path> stream = Files.newDirectoryStream(dir)){
//			for(Path file:stream) {
//				files += file.getFileName() + "\n";
//			}
//		} catch (IOException e) {
//			System.out.println(e.getMessage());
//		}
//		return files;
//	}
	
	//Retrieve the user's file list from the database
	@Override
	public synchronized ArrayList<String> retrieveFileListFromDB(int user_id) throws RemoteException {
		return db_manager.getUserFiles(user_id);
	}
	
}
