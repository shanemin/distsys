package jarg.networking.dfs.DFS_Backend.general.clientUtils;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/*
 * This interface will define operations by the master for the client.
 */
public interface ClientRequests_Intf extends Remote {
	
	public int createAccount(String username, String password) throws RemoteException;  //Create an account and get an id back to identify yourself in other requests
	public int logIn(String username, String password) throws RemoteException;			//Log in and get back an identifier to use for subsequent requests to identify yourself
	public String donwloadRedirect(int user_id, String filename) throws RemoteException;	//Ask the master where to download this file from
	public String uploadRedirect(int user_id, String filename) throws RemoteException;					//Ask the master where to upload a file
	public String deleteRedirect(int user_id, String filename) throws RemoteException;	//Ask the master where to delete a file
	//public String retrieveFileList(int userID) throws RemoteException;				//Retrieve the user's file list by searching in storage
	public ArrayList<String> retrieveFileListFromDB(int user_id) throws RemoteException; //Retrieve the user's file list from the database
} 
