package jarg.networking.dfs.DFS_Backend.general.clientUtils;

/*
 * This class will represent a document of the user.
 */


import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;


public class Document implements Serializable{
	private static final long serialVersionUID = 1L;
	private String filename;
	private byte[] data;
	
	//The boolean can be used in case you also wish to load the file's binary data into memory and have it serialized
	public Document(int userID, String filename, Path filepath, boolean loadData) {
		this.filename = filename;
		if(loadData) {
			try {
				//The maximum size with this code is limited by the JVM
				data = Files.readAllBytes(filepath);
			} catch (IOException e) {
				System.out.println(e.getMessage());
			} catch (OutOfMemoryError e) {
				System.out.println("File Too Large : " + e.getMessage());
			}
		}else {
			data = null;
		}
	}

	public String getFilename() {
		return filename;
	}
	

	public byte[] getData() {
		return data;
	}
	
}
