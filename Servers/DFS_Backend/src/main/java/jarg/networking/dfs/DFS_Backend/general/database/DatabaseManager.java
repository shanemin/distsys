package jarg.networking.dfs.DFS_Backend.general.database;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Random;

import org.apache.commons.dbcp.BasicDataSource;

import jarg.networking.dfs.DFS_Backend.general.DefaultValues;
import jarg.networking.dfs.DFS_Backend.general.ServerNode;

public class DatabaseManager {
	private BasicDataSource ds;
	private Connection connection;
	private ServerNode server_instance;
	
	public DatabaseManager(ServerNode server) {
		server_instance = server;
		ds = new BasicDataSource();
		Properties ds_prop = new Properties();
		try(FileInputStream ds_prop_input = new FileInputStream(DefaultValues.RESOURCES_FOLDER + "db_connection.properties")) {
			// load a properties file
			ds_prop.load(ds_prop_input);
			// get the property value and print it out
			ds.setDriverClassName(ds_prop.getProperty("jdbc.driver"));
			ds.setUsername(ds_prop.getProperty("jdbc.username"));
			ds.setPassword(ds_prop.getProperty("jdbc.password"));
			ds.setUrl(ds_prop.getProperty("jdbc.url"));
			connection = ds.getConnection();
			
		} catch (IOException | SQLException io) {
			System.out.println("Unalbe to connect to database");
			System.err.println(io.getMessage());
		}
	}
	 
	//Master functions ===============================================================================
	
	//Client creates account
	public int createAccount(String username, String password) {
		try {
			PreparedStatement prepstm = connection.prepareStatement("INSERT INTO DFS_db.users (username, password) VALUES (? , ?) ;");
			prepstm.setString(1, username);
			prepstm.setString(2, password);
			prepstm.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Failure in creating user.");
			System.err.println(e.getMessage());
		}
		return userLogIn(username, password);
	}
	
	
	//Returns to a user its id on log in or -1 if not found
	public int userLogIn(String username, String password) {
		ResultSet results;
		try {
			PreparedStatement prepstm = connection.prepareStatement("SELECT _id FROM DFS_db.users WHERE username=? AND password=? ;");
			prepstm.setString(1, username);
			prepstm.setString(2, password);
			results = prepstm.executeQuery();
			while(results.next()) {
				return results.getInt(1); 
			}
		} catch (SQLException e) {
			System.out.println("Failure in loggin in user.");
			System.err.println(e.getMessage());
		}
		return -1;
	}
	
	//Get a list of all the files of a user
	public ArrayList<String> getUserFiles(int id) {
		ArrayList<String> files = new ArrayList<>();
		ResultSet results;
		try {
			PreparedStatement prepstm = connection.prepareStatement("SELECT filename FROM DFS_db.files WHERE user_id=? ;");
			prepstm.setInt(1, id);
			results = prepstm.executeQuery();
			while(results.next()) {
				files.add( results.getString(1));
			}
		} catch (SQLException e) {
			System.out.println("Failure in getting user's files.");
			System.err.println(e.getMessage());
		}
		
		return files;
	}
	
	//Get which servers have the given file for a user
	//Random selection of one of these servers
	public String getServerForFile(int user_id, String filename) {
		ArrayList<String> servers = new ArrayList<>();
		ResultSet results;
		try {
			PreparedStatement prepstm = connection.prepareStatement("SELECT server_1_address, server_2_address FROM DFS_db.files WHERE user_id=? AND filename=? ;");
			prepstm.setInt(1, user_id);
			prepstm.setString(2, filename);
			results = prepstm.executeQuery();
			if(!results.next()) {	//no servers were found
				return "";
			}else {
				servers.add( results.getString(1));
				servers.add( results.getString(2));
			}
		} catch (SQLException e) {
			System.err.println("Failure in getting servers for a user's file.");
			System.err.println(e.getMessage());
			return "";
		}
		Random rand = new Random();
		int server_index = rand.nextInt(servers.size());
		return servers.get(server_index); //retrieve first server for now
	}
	
	//Slave functions ===========================================================================================
	
	//Original Files Management ******************************
	
	//Check if file exists already
	private boolean fileExists(int user_id, String filename) {
		boolean exists = false;
		ResultSet results;
		try {
			PreparedStatement prepstm = connection.prepareStatement("SELECT _id FROM DFS_db.files WHERE user_id = ? AND filename = ? ;");
			prepstm.setInt(1, user_id);
			prepstm.setString(2, filename);
			results = prepstm.executeQuery();
			while(results.next()) {
				exists = true;
				break;
			}
		} catch (SQLException e) {
			System.out.println("Failure in getting user's files.");
			System.err.println(e.getMessage());
		}
		return exists;
	}
	
	//This should be called by the server that stores the original file
	//The server that will store the replica should use another function
	public void storeUploadedFileInfo(int user_id, String filename, long size) {
		if(! fileExists(user_id, filename)) {
			try {
				PreparedStatement prepstm = connection.prepareStatement("INSERT INTO DFS_db.files (user_id, filename, size, server_1_address) VALUES ( ?, ?, ?, ? );");
				prepstm.setInt(1, user_id);
				prepstm.setString(2, filename);
				prepstm.setLong(3, size);
				String address = server_instance.getIp() + ":" + String.valueOf(server_instance.getPort());
				prepstm.setString(4, address);
				System.out.println("Uploaded file " + filename + " of user " + user_id + " in address : "+ address);
				prepstm.executeUpdate();
			} catch (Exception e) {
				System.err.println("Failure in storing uploaded file's info to database.");
				System.err.println(e.getMessage());
				System.err.println(e.getStackTrace());
			}
		}
	}
	
	//Update file's size when a new version is uploaded
	public void updateFileSize(int user_id, String filename, long size) {
		try {
			PreparedStatement prepstm = connection.prepareStatement("UPDATE DFS_db.files SET size=? WHERE user_id=? AND filename=?;");
			prepstm.setLong(1, size);
			prepstm.setInt(2, user_id);
			prepstm.setString(3, filename);
			prepstm.executeUpdate();
		} catch (Exception e) {
			System.err.println("Failure in updating file's size in database.");
			System.err.println(e.getMessage());
			System.err.println(e.getStackTrace());
		}
	}
	
	//This should be called by the server that deletes the original file
	//The server that will delete the replica should use another function
	public void deleteFileInfo(int user_id, String filename) {
		try {
			PreparedStatement prepstm = connection.prepareStatement("DELETE FROM DFS_db.files WHERE user_id=? AND filename=? ;");
			prepstm.setInt(1, user_id);
			prepstm.setString(2, filename);
			prepstm.executeUpdate();
			String address = server_instance.getIp() + ":" + String.valueOf(server_instance.getPort());
			System.out.println("Deleted file " + filename + " of user " + user_id + " in address : "+ address);
		} catch (SQLException e) {
			System.err.println("Failure in storing deleted file's info to database.");
			System.err.println(e.getMessage());
		}
	}
	
	
	//Replica Files Management ***************************************
	
	//This should be called by the server that stores the replica file
	//The server that will store the original should use another function
	public void replicaStoreUploadedFileInfo(int user_id, String filename) {
		try {
			PreparedStatement prepstm = connection.prepareStatement("UPDATE DFS_db.files SET server_2_address=? WHERE user_id = ? AND filename = ? ;");
			String address = server_instance.getIp() + ":" + server_instance.getPort();
			prepstm.setString(1, address);
			prepstm.setInt(2, user_id);
			prepstm.setString(3, filename);
			prepstm.executeUpdate();
			System.out.println("Uploaded replica file " + filename + " of user " + user_id + " in address : "+ address);
		} catch (SQLException e) {
			System.err.println("Replication : failure in updating uploaded file's info to database.");
			System.err.println(e.getMessage());
		}
	}
	
	//Retrieve all files that a server has. Can be used by siblings
	//For every user store a list with its files
	//The Map will have user ids and their files stored by this server that we asked about
	public Map<Integer, ArrayList<String>> getAllFilesPerUserForServer(String address){
		Map<Integer, ArrayList<String>> users_files = new HashMap<Integer, ArrayList<String>>();
		ResultSet results;
		ArrayList<String> files;
		int prev_user_id = -1;
		int curr_user_id;
		String filename;
		try {
			PreparedStatement prepstm = connection.prepareStatement("SELECT user_id, filename FROM DFS_db.files WHERE server_1_address=? OR server_2_address=? ORDER BY user_id ASC;"); 
			prepstm.setString(1, address);
			prepstm.setString(2, address);
			results = prepstm.executeQuery();
		    files = new ArrayList<String>();
			while(results.next()) {
				curr_user_id = results.getInt(1);
				filename = results.getString(2);
				//System.out.println("User : " + curr_user_id + " file : " + filename );
				if(curr_user_id != prev_user_id) { //we switched user
					if(prev_user_id != -1) {	//we now have all the files for this user. Store before changes
						users_files.put(Integer.valueOf(prev_user_id), files); 
					}
					files = new ArrayList<String>();
					files.add(filename);
				}else {
					files.add(filename);
				}
				prev_user_id = curr_user_id;
			}
			users_files.put(Integer.valueOf(prev_user_id), files); 
		}catch(SQLException e) {
			System.err.println("Error in getting all files from user : " + e.getMessage());
		}
//		//Debugging
//		System.out.println("Printing files for sibling : "+address);
//		for(Entry<Integer, ArrayList<String>> entr : users_files.entrySet()) {
//			System.out.println(entr.getKey());
//			for(String file : entr.getValue()) {
//				System.out.println("    "+file);
//			}
//		}
		return users_files;
	}
	
	

}
