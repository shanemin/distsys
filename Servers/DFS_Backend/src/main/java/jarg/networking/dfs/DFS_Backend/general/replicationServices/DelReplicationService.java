/*
 * Used to delete a replicated server's file from the sibling, using a thread
 */
package jarg.networking.dfs.DFS_Backend.general.replicationServices;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import jarg.networking.dfs.DFS_Backend.general.ServerInfo;

public class DelReplicationService implements Runnable {
	private int userID;
	private String filename;
	private ServerInfo replica_server_info;
	
	public DelReplicationService(int userID, String filename, ServerInfo replica_server_info) {
		this.userID = userID;
		this.filename = filename;
		this.replica_server_info = replica_server_info;
	}

	@Override
	public void run() {
		try {
			Registry registry = LocateRegistry.getRegistry(replica_server_info.getIp(), replica_server_info.getPort());
			Replicator_Intf replica_connector = (Replicator_Intf) registry.lookup("Replicator");
			replica_connector.deleteReplica(userID, filename);
		} catch (RemoteException | NotBoundException e) {
			System.err.println(e.getMessage());
		}

	}

}