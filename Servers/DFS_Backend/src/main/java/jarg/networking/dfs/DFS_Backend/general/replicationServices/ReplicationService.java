/*
 * Used to replicate a server's file to its replica sibling by a thread
 */
package jarg.networking.dfs.DFS_Backend.general.replicationServices;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import jarg.networking.dfs.DFS_Backend.general.ServerInfo;
import jarg.networking.dfs.DFS_Backend.general.clientUtils.Document;

public class ReplicationService implements Runnable {
	private Document doc;
	private int user_id;
	private ServerInfo replica_server_info;
	
	public ReplicationService(int user_id, Document doc, ServerInfo replica_server_info) {
		this.user_id = user_id;
		this.doc = doc;
		this.replica_server_info = replica_server_info;
	}

	@Override
	public void run() {
		try {
			Registry registry = LocateRegistry.getRegistry(replica_server_info.getIp(), replica_server_info.getPort());
			Replicator_Intf replica_connector = (Replicator_Intf) registry.lookup("Replicator");
			replica_connector.saveReplica(user_id, doc);
		} catch (RemoteException | NotBoundException e) {
			System.err.println(e.getMessage());
		}

	}

}
