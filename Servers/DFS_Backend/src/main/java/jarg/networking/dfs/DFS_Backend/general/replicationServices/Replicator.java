package jarg.networking.dfs.DFS_Backend.general.replicationServices;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.RemoteException;

import jarg.networking.dfs.DFS_Backend.general.ServerNode;
import jarg.networking.dfs.DFS_Backend.general.clientUtils.Document;
import jarg.networking.dfs.DFS_Backend.general.database.DatabaseManager;

public class Replicator implements Replicator_Intf {
	private static String DEFAULT_FOLDER = "Client_Files"+File.separator;
	private ServerNode server_instance;
	private DatabaseManager db_manager;
	
	public Replicator(ServerNode server_instance, DatabaseManager db_manager) {
		this.server_instance = server_instance;
		this.db_manager = db_manager;
		DEFAULT_FOLDER += "S_"+server_instance.getIp()+"_"+server_instance.getPort()+File.separator;
	}

	@Override
	public void saveReplica(int user_id, Document doc) {
		if(doc != null) {
			String file_path = DEFAULT_FOLDER + "User"+ String.valueOf(user_id)+File.separator;
			try {
				Files.createDirectories(Paths.get(file_path));
			}catch( FileAlreadyExistsException e) {
				//No need to print anything
			}catch (IOException e) {
				System.out.println(e.getMessage());
			}
			file_path += doc.getFilename();
			//Check if file already exists
			Path check_path = Paths.get(file_path);
			boolean already_exists = Files.exists(check_path);
			long previous_size = 0;
			if(already_exists) {
				try {
					previous_size = Files.size(check_path);
				} catch (IOException e) {
					System.err.println("Error in obtaining previous file size");
				}
			}
			try(FileOutputStream out_file = new FileOutputStream(file_path)){
				//Save on your own storage
    			out_file.write(doc.getData());
    			//Update your status
    			if(!already_exists) { //update this info only if it's a new file
    				//Update database
        			db_manager.replicaStoreUploadedFileInfo(user_id, doc.getFilename());
    				//update your status
        			server_instance.addLoad(doc.getData().length);
        			//notify parent of load change if needed
        			server_instance.decideOnLeastLoad();
    			}else {
    				long new_size = doc.getData().length;
    				long difference = new_size - previous_size;
    				if(difference == 0) {
    					System.out.println("The new version of the file was stored. The load remains the same.");
    				}else {
    					System.out.println("The new version of the file was stored.");
    					//update your status
            			server_instance.addLoad(difference);
            			//notify parent of load change if needed
            			server_instance.decideOnLeastLoad();
    				}
    			}    		}catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}else {
			System.out.println("Invalid upload request : empty file");
		}
	}
	
	public void deleteReplica(int user_id, String filename) throws RemoteException{
		String filepath = DEFAULT_FOLDER + "User"+ String.valueOf(user_id)+ File.separator + filename;
		Path path = Paths.get(filepath);
		if(Files.exists(path)) {
			try {
				long file_size = Files.size(path);
				Files.delete(path);
				//update your status
    			server_instance.subtractLoad(file_size);
    			//notify parent of load change if needed
    			server_instance.decideOnLeastLoad();
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}
	}

}
