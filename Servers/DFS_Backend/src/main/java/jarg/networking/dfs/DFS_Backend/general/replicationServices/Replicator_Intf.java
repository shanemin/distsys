package jarg.networking.dfs.DFS_Backend.general.replicationServices;

/*
 * This interface is used to define methods that a slave node will
 * call when it should handle replicas of files. They will be called
 * remotely by their siblings to maintain consistency.
 */


import java.rmi.Remote;
import java.rmi.RemoteException;

import jarg.networking.dfs.DFS_Backend.general.clientUtils.Document;

public interface Replicator_Intf extends Remote {
	public void saveReplica(int user_id, Document doc) throws RemoteException;
	public void deleteReplica(int user_id, String filename) throws RemoteException;
}
