/*
 * This class will be used by the master node. It will manage most operations
 * regarding connecting new servers in a topology and keeping a list
 * with information about all the servers connected.
 * 
 */
package jarg.networking.dfs.DFS_Backend.master_node;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.RemoteServer;
import java.rmi.server.ServerNotActiveException;

import jarg.networking.dfs.DFS_Backend.general.*;
import jarg.networking.dfs.DFS_Backend.slave_node.SlaveConnector_Intf;
import java.util.ArrayList;
import java.util.Iterator;

public class MasterConnector implements Connector_Intf {

    private TopologyConstructor_Intf network_topology;
    private MasterNode master_instance;

    public MasterConnector(MasterNode master_instance) {
        this.master_instance = master_instance;
        network_topology = new TopologyConstructor();
        master_instance.getConnections().add(master_instance);		//put master as the first in the list
    }

    @Override
    //This function will be called by any server that wishes to connect to this one.
    //The server passes which port does its own registry listen to.
    //The master registers the server and returns to it its parent in the topology.
    public synchronized ServerInfo connect(int slave_registry_port) throws RemoteException {
        ServerInfo parent = null;
        try {
            //Register the new slave node
            String slave_ip = RemoteServer.getClientHost();
            ServerInfo new_slave = new ServerInfo(slave_ip, slave_registry_port, false);

            //If the node is a revived node, reconnect it to its original position in the tree
            for (ServerInfo node : master_instance.getConnections()) {
                if (node.getPort() == slave_registry_port && node.getIp().equals(slave_ip)) {
                    System.out.println("Reconnected:\t Slave node <" + node.getIp() + " , " + node.getPort() + 
                            ">. Parent is <" + node.getParent().getIp() + " , " + node.getParent().getPort() + ">");
                    node.setStatus(true);
                    return node.getParent();
                }
            }

            master_instance.getConnections().add(new_slave);
            int connections_size = master_instance.getConnections().size();
            if ((connections_size < 4)) {
                master_instance.addChild(new_slave);
                master_instance.setLeastLoadNode(new_slave);	//also update to the latest child initially
            }
            //Set the new node's parent ***
            //Full information for master 
            new_slave.setParent(network_topology.getParentInNetwork(master_instance.getConnections()));
            //the slave node only needs this
            parent = new ServerInfo(new_slave.getParent().getIp(), new_slave.getParent().getPort(), new_slave.getParent().isMaster());
            //Connect to your children and pass them their siblings
            if ((master_instance.getChildren().size() > 1) && (connections_size < 4)) {
                for (int i = 0; i < DefaultValues.MAX_CHILDREN; i++) {
                    Registry child_registry;
                    SlaveConnector_Intf child_connector;
                    try {
                        child_registry = LocateRegistry.getRegistry(master_instance.getChildren().get(i).getIp(), master_instance.getChildren().get(i).getPort());
                        child_connector = (SlaveConnector_Intf) child_registry.lookup("Child_Connector");
                        for (int j = 0; j < DefaultValues.MAX_CHILDREN; j++) {
                            if (i != j) { //not yourself
                                child_connector.sendWhichIsTheReplicaSibling(master_instance.getChildren().get(j));
                            }
                        }
                    } catch (Exception e) {
                        System.err.println(e.getMessage());
                        System.err.println(e.getStackTrace());
                    }
                }
            }
            System.out.println("Connected:\t Slave node <" + slave_ip + " , " + slave_registry_port + ">."
                    + " Parent is <" + new_slave.getParent().getIp() + " , " + new_slave.getParent().getPort() + ">");
        } catch (ServerNotActiveException e) {
            System.err.println(e.getMessage());
        }
        return parent;

    }

    @Override
    //This function will be used by a server to notify its parent when a change in its load occurs
    public synchronized void notifyOnLoadChange(int caller_port, ServerInfo candidate) throws RemoteException {
        boolean valid_call = false;	//was it called by a child?
        String caller_ip = "";
        try {
            caller_ip = RemoteServer.getClientHost();
        } catch (ServerNotActiveException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //Update this child's load 
        for (ServerInfo child : master_instance.getChildren()) {
            if (child.isIdentical(caller_ip, caller_port)) {
                child.setLeastLoadNode(candidate);
                valid_call = true;
                break;
            }
        }
        if (valid_call) {
            System.out.println("Received notify from child with Ip : " + caller_ip + " port : " + caller_port + " with candidate : " + candidate.toString());
            master_instance.decideOnLeastLoad();
        } else {
            System.err.println("Somebody who's not my child called \"notify\". Ip : " + caller_ip + " port : " + caller_port);
        }

    }

    @Override
    public ArrayList<ServerInfo> getServerList() throws RemoteException {
        return this.master_instance.getConnections();
    }

}
