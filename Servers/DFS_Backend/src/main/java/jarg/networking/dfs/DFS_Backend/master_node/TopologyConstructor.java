/*
 * Returns the parent of a node in a tree topology where each
 * node has N children. It is used to calculate where to put
 * a new server in the tree.
 */

package jarg.networking.dfs.DFS_Backend.master_node;

import java.util.ArrayList;

import jarg.networking.dfs.DFS_Backend.general.ServerInfo;

public class TopologyConstructor implements TopologyConstructor_Intf {
	private static int children_per_node;	//how many children will each node have
	private static int parent_index;		//the index of the last selected parent node inside a list of nodes.
	
	static {
		children_per_node = 2;
		parent_index = 0; //point to the master node for now
	}
	
	public TopologyConstructor() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * This decides which will be the parent node for a new node.
	 * When a new node asks to connect, this method will assign to it 
	 * the parent as indicated by parent_index.
	 * After this, it will check if the new node's index in the list of
	 * all nodes is a multiple of children_per_node. If so, it will update
	 * the parent_index by adding 1. That way the nodes will be connected
	 * in a tree where each node will have children_per_node children.
	 */
	@Override
	public synchronized ServerInfo getParentInNetwork(ArrayList<ServerInfo> network) {
		ServerInfo parent = network.get(parent_index);
		int nodes = network.size();
		if(((nodes-1) > 0) && ((nodes-1) % children_per_node) == 0 ) {
			parent_index ++;
		}
		return parent;
	}

}
