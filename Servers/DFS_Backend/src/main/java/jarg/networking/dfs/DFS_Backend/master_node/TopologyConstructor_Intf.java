/*
 * Implement this interface to define how the nodes are connected to form the topology.
 * It should be used to return to a server its parent node.
 */

package jarg.networking.dfs.DFS_Backend.master_node;

import java.util.ArrayList;

import jarg.networking.dfs.DFS_Backend.general.ServerInfo;

public interface TopologyConstructor_Intf {
	public ServerInfo getParentInNetwork(ArrayList<ServerInfo> network);
}
