/*
 * This class is used by a slave node to allow connections of its
 * children. Assumes a binary tree connection.
 */
package jarg.networking.dfs.DFS_Backend.slave_node;

import java.io.FileOutputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.RemoteServer;
import java.rmi.server.ServerNotActiveException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import jarg.networking.dfs.DFS_Backend.general.DefaultValues;
import jarg.networking.dfs.DFS_Backend.general.ServerInfo;
import jarg.networking.dfs.DFS_Backend.general.ServerNode;
import jarg.networking.dfs.DFS_Backend.general.clientUtils.ClientRedirectedRequests;
import jarg.networking.dfs.DFS_Backend.general.clientUtils.ClientRedirectedRequests_Intf;
import jarg.networking.dfs.DFS_Backend.general.clientUtils.Document;
import jarg.networking.dfs.DFS_Backend.general.database.DatabaseManager;

public class SlaveConnector implements SlaveConnector_Intf {
	private DatabaseManager db_manager; //this will handle all database operations
    private ServerNode slave_instance;	//will be passed by the class that created an instance for this slave

    public SlaveConnector(ServerNode slave_instance, DatabaseManager db_manager) {
        this.slave_instance = slave_instance;
        this.db_manager = db_manager;
    }

    @Override
    //This function will be called by any server that wishes to connect to this one.
    //The server passes which port does its own registry listen to.
    public synchronized ServerInfo connect(int slave_registry_port) throws RemoteException {
        try {
            String slave_ip = RemoteServer.getClientHost();
            ServerInfo new_slave = new ServerInfo(slave_ip, slave_registry_port, false);
            slave_instance.addChild(new_slave);

            //Connect to your children and pass them their siblings
            if (slave_instance.getChildren().size() > 1) {
                for (int i = 0; i < DefaultValues.MAX_CHILDREN; i++) {
                    Registry child_registry;
                    SlaveConnector_Intf child_connector;
                    try {
                        child_registry = LocateRegistry.getRegistry(slave_instance.getChildren().get(i).getIp(), slave_instance.getChildren().get(i).getPort());
                        child_connector = (SlaveConnector_Intf) child_registry.lookup("Child_Connector");
                        for(int j=0; j<DefaultValues.MAX_CHILDREN; j++){
                        	if( i != j) { //not yourself
                        		child_connector.sendWhichIsTheReplicaSibling(slave_instance.getChildren().get(j));
                        	}
                        }
                    } catch (Exception e) {
                        System.err.println(e.getMessage());
                        System.err.println(e.getStackTrace());
                    }
                }
            }

            System.out.println("Child node <" + slave_ip + " , " + slave_registry_port + "> connected.");
        } catch (ServerNotActiveException e) {
            System.err.println(e.getMessage());
        }
        return null; //no extra info needed in this case
    }

    //Parents call this to pass to the children the list of siblings
    //The implementation here defines how this server adds new siblings to a list, after a message from the parent
    @Override
    public synchronized void sendWhichIsTheReplicaSibling(ServerInfo sibling) throws RemoteException {
        try {
            slave_instance.getSiblings().add(sibling);
            System.out.println("Added sibling <" + sibling.getIp() + "," + sibling.getPort() + ">");
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println(e.getStackTrace());
        }

    }

	@Override
	public synchronized void notifyOnLoadChange(int caller_port, ServerInfo candidate) throws RemoteException {
		boolean valid_call = false;	//was it called by a child?
		String caller_ip = "";
		try {
			caller_ip = RemoteServer.getClientHost();
		} catch (ServerNotActiveException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Update this child's load 
		for(ServerInfo child : slave_instance.getChildren()) {
			 if(child.isIdentical(caller_ip, caller_port)) {
				 child.setLeastLoadNode(candidate);
				 valid_call = true;
				 break;
			 }
		 }
		if(valid_call) {
			System.out.println("Received notify from child with Ip : "+caller_ip + " port : "+ caller_port + " with candidate : " + candidate.toString());
			slave_instance.decideOnLeastLoad();
		}else {
			System.err.println("Somebody who's not my child called \"notify\". Ip : "+caller_ip+" port : "+caller_port );
		}
		
	}

    @Override
    public ArrayList<ServerInfo> getServerList() throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
