package jarg.networking.dfs.DFS_Backend.slave_node;

import java.rmi.RemoteException;

import jarg.networking.dfs.DFS_Backend.general.Connector_Intf;
import jarg.networking.dfs.DFS_Backend.general.ServerInfo;

public interface SlaveConnector_Intf extends Connector_Intf {
	//Parents call this to pass to the children the list of siblings
	public void sendWhichIsTheReplicaSibling(ServerInfo sibling) throws RemoteException;
}
